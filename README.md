*Comenzando 🚀

	Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas.
	Mira Deployment para conocer como desplegar el proyecto.

*Pre-requisitos 📋

	-Node.js (Versión 12.x LTS) - https://nodejs.org/es/ Puede guiarse para la instalación en https://www.youtube.com/watch?v=PLD8sZPevEU
	-Visual Studio Code - https://code.visualstudio.com/ Puede guiarse para la instalación en https://www.youtube.com/watch?v=zlqUhCCLPnE

*Instalación 🔧

	-Para instalar Node.js puede guiarse para la instalación en https://www.youtube.com/watch?v=PLD8sZPevEU
	-Para la instalacion de Visual Studio Code puede guiarse para la instalación en https://www.youtube.com/watch?v=zlqUhCCLPnE

*Ejecutando las pruebas ⚙️

	1.- Para probar desde el VSCode, vamos a levantar nuestro proyecto, para ellos hacemos lo siguiente, de otra manera pasamos al numero 2:
		
		-Abrimos Visual Studio Code.
		-Antes que nada, debemos descargas las dependencias en nuestro proyecto, para ello presionamos ctrl + ñ, esto abrira una consola en nuestro VS Code.
		-Ahi ejecutamos el siguiente comando: npm install -g @angular/cli. Ahora procedemos a ejecutar este comando: npm install. 
		-De aqui en adelante ya podemos levantar nuestro proyecto; para eso ejecutamos el comando ng s -o. 
		*Nota: debemos encontrarnos en la carpeta de nuestro proyecto para ejecutar estos comandos; moverse entre carpetas 
		dentro de la consola para eso.*
	
	2.- Una vez hecho esto se nos abrira una pestaña en nuestro navegador; del lado izquierdo hay una barra desplegable donde hay dos botones, organizacion y chargepoint. Al dar clic en alguna de ellos se abrira una ventana con una tabla con la informacion que tengamos en la BD, de otra manera se vera todo en blanco.

		-Registrar->Presionamos el boton circular en la parte inferior, eso nos abrira una ventana con los campos a llenar para un nuevo registro.

		Por cada fila de elementos en la tabla, se podran ver dos botones, modificar y eliminar.

		-Modificar->Si damos clic al boton de modificar se abrira una ventana con los datos de ese registro a modificar, ahi se modifican los datos requeridos.
		-Eliminar->Se presiona el boton de eliminar, esto borrara el registro de la base de datos.

		*Todas estas acciones se podran visualizar de manera inmediata, ya que al momento de aceptar el registro nuevo, cambio o eliminacion, la tabla se actualizara.*

		-Buscar registro->En la parte superior de la tabla, hay un campo en el cual se puede buscar un registro en especifico.


*Despliegue 📦
	
	En VSCode abrimos una consola presionando CTRL + ñ y colocamos -ng build --prod
	
	Abrimos una consola, nos dirigimos a la ruta donde se encuentra nuestro proyecto front end, a la altura donde tenemos nuestra carpeta dist y en ella colocamos el siguiente comando:
	
	-docker build -t av-app-image .
	-docker run --name av-app-container -d -p 8090:90 av-app-image
	-En cualquier navegador colocar la URL http://localhost:8090/

*Construido con 🛠️

	-Material Design