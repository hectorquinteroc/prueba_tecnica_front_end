import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GenericService<T> {

  constructor(
    protected http: HttpClient,
    @Inject(String) protected url: string
  ) { }

  list(){
    console.log(this.url);
    return this.http.get<T[]>(this.url);
  }

  listId(id: string) {
    console.log(this.url);
    return this.http.get<T>(`${this.url}/${id}`);
  }

  record(t: T) {
    console.log(this.url);
    return this.http.post(this.url, t);
  }

  update(t: T) {
    console.log(this.url);
    return this.http.put(this.url, t);
  }

  delete(id: string) {
    console.log(this.url);
    return this.http.delete(`${this.url}/${id}`);
  }

}
