import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { GenericService } from './generic.service';
import { ChargePoint } from './../_model/chargePoint';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChargeService extends GenericService<ChargePoint>{

  private chargePointCambio = new Subject<ChargePoint[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected http: HttpClient) {
    super(
      http,
      `${environment.HOST}/chargepoint`
    );
  }

  listPageable(p: number, s:number){
    return this.http.get<any>(`${this.url}/pageable?page=${p}&size=${s}`);
  }

  getCharge(){
    return this.chargePointCambio.asObservable();
  }

  setCharge(chargePoint: ChargePoint[]){
    this.chargePointCambio.next(chargePoint);
  }

  getMessage(){
    return this.mensajeCambio.asObservable();
  }

  setMessage(mensaje: string){
    return this.mensajeCambio.next(mensaje);
  }
}
