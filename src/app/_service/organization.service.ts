import { GenericService } from './generic.service';
import { Organization } from './../_model/organization';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrganizationService extends GenericService<Organization>{

  private organizationCambio = new Subject<Organization[]>();
  private mensajeCambio = new Subject<string>();

  constructor(protected http: HttpClient) {
    super(
      http,
      `${environment.HOST}/organization`
    );
  }

  listPageable(p: number, s:number){
    return this.http.get<any>(`${this.url}/pageable?page=${p}&size=${s}`);
  }

  getOrganization(){
    return this.organizationCambio.asObservable();
  }

  setOrganization(organization: Organization[]){
    this.organizationCambio.next(organization);
  }

  getMessage(){
    return this.mensajeCambio.asObservable();
  }

  setMessage(mensaje: string){
    return this.mensajeCambio.next(mensaje);
  }
}
