import { ChargeEditionComponent } from './pages/charge/charge-edition/charge-edition.component';
import { OrganizationEditionComponent } from './pages/organization/organization-edition/organization-edition.component';

import { OrganizationComponent } from './pages/organization/organization.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChargeComponent } from './pages/charge/charge.component';

const routes: Routes = [
  { 
    path: 'organization', component: OrganizationComponent,children: [
      { path: 'new', component: OrganizationEditionComponent },
      { path: 'edition/:id', component: OrganizationEditionComponent } ]
  },
  { 
    path: 'charge', component: ChargeComponent,children: [
      { path: 'new', component: ChargeEditionComponent },
      { path: 'edition/:id', component: ChargeEditionComponent } ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
