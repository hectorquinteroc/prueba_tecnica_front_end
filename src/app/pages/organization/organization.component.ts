import { Organization } from './../../_model/organization';
import { OrganizationService } from './../../_service/organization.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.css']
})
export class OrganizationComponent implements OnInit {

  lista : Organization[]  = [];
  displayedColumns = ['id', 'name', 'legalEntity', 'actions'];
  dataSource: MatTableDataSource<Organization>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  cantidad: number = 0;

  constructor(
    private organizationService: OrganizationService,
    private snackBar: MatSnackBar
    ) { }

  ngOnInit(): void {
    this.organizationService.getOrganization().subscribe(data => {
      console.log(data);
      this.lista = data;
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });

    this.organizationService.getMessage().subscribe(data => {
      this.snackBar.open(data, 'WARNING', { duration: 2000 });
    });

   this.organizationService.listPageable(0, 10).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort = this.sort;
    });
  }

  filtrar(valor: string) {
    this.dataSource.filter = valor.trim().toLowerCase();
  }

  mostrarMas(e: any) {
    this.organizationService.listPageable(e.pageIndex, e.pageSize).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort = this.sort;
    });
  }

  eliminar(idOrganization: string) {
    this.organizationService.delete(idOrganization).pipe(switchMap(() => {
      return this.organizationService.list()
    })).subscribe(data => {
      this.organizationService.setOrganization(data);
      this.organizationService.setMessage('SE ELIMINÓ');
    });
  }
}
