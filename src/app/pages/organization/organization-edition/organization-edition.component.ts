import { switchMap } from 'rxjs/operators';
import { OrganizationService } from './../../../_service/organization.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Organization } from 'src/app/_model/organization';
import { v4 as uuid } from 'uuid';

@Component({
  selector: 'app-organization-edition',
  templateUrl: './organization-edition.component.html',
  styleUrls: ['./organization-edition.component.css']
})
export class OrganizationEditionComponent implements OnInit {

  form: FormGroup;
  id: string;
  edicion: boolean;
  uuid: string = uuid();
  
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private organizationService: OrganizationService
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      'id': new FormControl(''),
      'name': new FormControl('', [Validators.required]),
      'legalEntity': new FormControl('')
    });

    this.route.params.subscribe((data: Params) => {
      this.id = data['id'];
      this.edicion = data['id'] != null;
      this.initForm();
    });
  }

  get f() { return this.form.controls; }

  private initForm() {
    if (this.edicion) {

      this.organizationService.listId(this.id).subscribe(data => {
        this.form = new FormGroup({
          'id': new FormControl(data.id),
          'name': new FormControl(data.name),
          'legalEntity': new FormControl(data.legalEntity),
        });

      });
    }
  }

  operar() {

    if (this.form.invalid) { return; }

    let organization = new Organization();
    //organization.id = this.form.value['id'];
    
    organization.name = this.form.value['name'];
    organization.legalEntity = this.form.value['legalEntity'];


    if (this.edicion) {
      //PRACTICA IDEAL
      organization.id = this.form.value['id'];
      this.organizationService.update(organization).pipe(switchMap(() => {
        return this.organizationService.list();
      })).subscribe(data => {
        this.organizationService.setOrganization(data);
        this.organizationService.setMessage('SE MODIFICÓ');
      });
    } else {
      //REGISTRAR
      //PRACTICA COMUN
      organization.id = this.uuid;
      this.organizationService.record(organization).subscribe(() => {
        this.organizationService.list().subscribe(data => {
          this.organizationService.setOrganization(data);
          this.organizationService.setMessage('SE REGISTRÓ');
        });
      });
    }

    this.router.navigate(['organization']);
  }
}
