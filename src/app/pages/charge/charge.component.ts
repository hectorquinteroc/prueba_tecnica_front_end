import { ChargeService } from './../../_service/charge.service';
import { ChargePoint } from './../../_model/chargePoint';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-charge',
  templateUrl: './charge.component.html',
  styleUrls: ['./charge.component.css']
})
export class ChargeComponent implements OnInit {

  displayedColumns = ['id', 'identity', 'actions'];
  dataSource: MatTableDataSource<ChargePoint>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  cantidad: number = 0;

  constructor(
    private chargeService: ChargeService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.chargeService.getCharge().subscribe(data => { 
      console.log(data);
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });

    this.chargeService.getMessage().subscribe(data => {
      this.snackBar.open(data, 'AVISO', { duration: 2000 });
    });

    this.chargeService.listPageable(0, 10).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort = this.sort;
      //this.dataSource.paginator = this.paginator;
    });
  }

  filtrar(valor: string) {
    this.dataSource.filter = valor.trim().toLowerCase();
  }

  mostrarMas(e: any) {
    this.chargeService.listPageable(e.pageIndex, e.pageSize).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
      this.dataSource.sort = this.sort;
    });
  }

  eliminar(idOrganization: string) {
    this.chargeService.delete(idOrganization).pipe(switchMap(() => {
      return this.chargeService.list()
    })).subscribe(data => {
      this.chargeService.setCharge(data);
      this.chargeService.setMessage('SE ELIMINÓ');
    });
  }
}
