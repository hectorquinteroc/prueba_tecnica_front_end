import { switchMap } from 'rxjs/operators';
import { ChargeService } from './../../../_service/charge.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ChargePoint } from 'src/app/_model/chargePoint';
import { v4 as uuid } from 'uuid';

@Component({
  selector: 'app-charge-edition',
  templateUrl: './charge-edition.component.html',
  styleUrls: ['./charge-edition.component.css']
})
export class ChargeEditionComponent implements OnInit {

  form: FormGroup;
  id: string;
  edicion: boolean;
  uuid: string = uuid();
  
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private chargeService: ChargeService
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      'id': new FormControl(''),
      'identity': new FormControl('')
    });

    this.route.params.subscribe((data: Params) => {
      this.id = data['id'];
      this.edicion = data['id'] != null;
      this.initForm();
    });
  }

  get f() { return this.form.controls; }

  private initForm() {
    if (this.edicion) {

      this.chargeService.listId(this.id).subscribe(data => {
        this.form = new FormGroup({
          'id': new FormControl(data.id),
          'identity': new FormControl(data.identity)
        });
      });
    }
  }

  operar() {

    if (this.form.invalid) { return; }

    let chargePoint = new ChargePoint();
    
    
    chargePoint.identity = this.form.value['identity'];


    if (this.edicion) {
      //PRACTICA IDEAL
      chargePoint.id = this.form.value['id'];
      this.chargeService.update(chargePoint).pipe(switchMap(() => {
        return this.chargeService.list();
      })).subscribe(data => {
        this.chargeService.setCharge(data);
        this.chargeService.setMessage('SE MODIFIC�');
      });
    } else {
      //REGISTRAR
      //PRACTICA COMUN
      chargePoint.id = this.uuid;
      this.chargeService.record(chargePoint).subscribe(() => {
        this.chargeService.list().subscribe(data => {
          this.chargeService.setCharge(data);
          this.chargeService.setMessage('SE REGISTR�');
        });
      });
    }

    this.router.navigate(['charge']);
  }
}
