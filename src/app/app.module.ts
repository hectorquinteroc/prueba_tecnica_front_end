import { MaterialModule } from './material/material/material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { OrganizationComponent } from './pages/organization/organization.component';
import { OrganizationEditionComponent } from './pages/organization/organization-edition/organization-edition.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ChargeComponent } from './pages/charge/charge.component';
import { ChargeEditionComponent } from './pages/charge/charge-edition/charge-edition.component';

@NgModule({
  declarations: [
    AppComponent,
    OrganizationComponent,
    OrganizationEditionComponent,
    ChargeComponent,
    ChargeEditionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
